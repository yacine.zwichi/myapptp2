package com.example.myapptp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class BookActivity extends AppCompatActivity {
    private Intent mIntent;
    private Book mBook;
    private EditText mEditTitle, mEditAuthors, mEditYear, mEditPublisher, mEditGenres;
    private Button mButtonSave;
    private BookDbHelper mBookDbHelper;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        mBookDbHelper = new BookDbHelper(this);

        mEditTitle = findViewById(R.id.nameBook);
        mEditAuthors = findViewById(R.id.editAuthors);
        mEditGenres = findViewById(R.id.editGenres);
        mEditYear = findViewById(R.id.editYear);
        mEditPublisher = findViewById(R.id.editPublisher);
        mButtonSave = findViewById(R.id.button);

        mIntent = getIntent();
        mBook = mIntent.getParcelableExtra(MainActivity.SELECTED_BOOK);

        //Condition
        if (mBook != null) {
            fillEditFields(mBook);
        }


        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBook == null) {
                    mBook = new Book();
                    UpdateBookFromEditFields(mBook);
                    if (mBook.getTitle().equals("")) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BookActivity.this);
                        builder1.setMessage("Saisir un titre ");
                        builder1.setCancelable(true);
                        builder1.show();
                        return;
                    }


                    if (!mBookDbHelper.addBook(mBook)) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BookActivity.this);
                        builder1.setMessage("Il existe déjà dans la base ! ");
                        builder1.setCancelable(true);
                        builder1.show();
                        return;

                    }


                } else {
                    UpdateBookFromEditFields(mBook);
                    if (mBook.getTitle().equals("")) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BookActivity.this);
                        builder1.setMessage("Ya pas de titre ! ");
                        builder1.setCancelable(true);
                        builder1.show();
                        return;
                    }
                    mBookDbHelper.updateBook(mBook);


                }
                Intent intent = new Intent(BookActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

    }

    public boolean fillEditFields(Book bookToFill) {
        if (bookToFill != null) {
            mEditTitle.setText(bookToFill.getTitle());
            mEditAuthors.setText(bookToFill.getAuthors());
            mEditYear.setText(bookToFill.getYear());
            mEditGenres.setText(bookToFill.getGenres());
            mEditPublisher.setText(bookToFill.getPublisher());
            return true;
        } else {
            return false;
        }
    }

    public boolean UpdateBookFromEditFields(Book bookToUpdate) {
        if (bookToUpdate != null) {
            bookToUpdate.setTitle(mEditTitle.getText().toString());
            bookToUpdate.setAuthors(mEditAuthors.getText().toString());
            bookToUpdate.setYear(mEditYear.getText().toString());
            bookToUpdate.setGenres(mEditGenres.getText().toString());
            bookToUpdate.setPublisher(mEditPublisher.getText().toString());
            return true;
        } else {
            return false;
        }
    }

}
