package com.example.myapptp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    //Création
    ListView mListView ;

    FloatingActionButton fab;

    BookDbHelper mBookDbHelper;

    MyCursorAdapter myCursorAdapter;

    Cursor mCursor;

    static final String SELECTED_BOOK = "Selected";

    @Override

    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mBookDbHelper = new BookDbHelper(this);
        mBookDbHelper.populate();
        mListView = findViewById(R.id.list_book);

        mCursor = mBookDbHelper.fetchAllBooks();

        myCursorAdapter = new MyCursorAdapter(this, mCursor,1);

        mListView.setAdapter(myCursorAdapter);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCursor.moveToPosition(position);
                Book selectedBook = mBookDbHelper.cursorToBook(mCursor);
                Intent intent = new Intent(MainActivity.this,BookActivity.class);
                intent.putExtra(SELECTED_BOOK, selectedBook);
                startActivity(intent);

            }
        });

        registerForContextMenu(mListView);

        mListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            // We want to create a context Menu when the user long click on an item
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(Menu.NONE, 1, Menu.NONE, "Supprimer");
            }



        });

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,BookActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        mCursor.moveToPosition(info.position);
        mBookDbHelper.deleteBook(mCursor);
        mCursor = mBookDbHelper.fetchAllBooks();
        myCursorAdapter.changeCursor(mCursor);
        myCursorAdapter.notifyDataSetChanged();
        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    protected void onResume() {
        super.onResume();
        mCursor = mBookDbHelper.fetchAllBooks();
        myCursorAdapter.changeCursor(mCursor);
        myCursorAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
